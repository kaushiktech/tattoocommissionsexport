﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tattoo_Commissions_Export.DBImplementation;
using Tattoo_Commissions_Export.Data.DataObjects;
using Tattoo_Commissions_Export.ApiImplementation;
using Tattoo_Commissions_Export.Data;
using System.IO;

namespace Tattoo_Commissions_Export
{
    public partial class frmEditConfiguration : Form
    {
        public string Mode = string.Empty;
        public Merchant Merchant;
        public frmEditConfiguration()
        {
            InitializeComponent();
        }

        private void EditConfiguration_Load(object sender, EventArgs e)
        {

            switch (Mode)
            {
                case "Inventory":
                    this.Text = "Edit Clover Inventory for Store: " + Merchant.Name;
                    List<Item> itemList = DBFunc.FetchCloverInventory(Merchant.Id);
                    CloverData.MerchantId = Merchant.Id;
                    IList<Item> cloverItems = CloverData.FetchCloverInventory();
                    foreach (Item item in cloverItems)
                    {
                        item.MerchantId = Merchant.Id;
                        if (itemList.Count == 0 || itemList.Count(i => i.Id == item.Id) == 0)
                            DBFunc.InsertCloverItem(item);
                    }
                    itemList = DBFunc.FetchCloverInventory(Merchant.Id);
                    var column = new DataGridViewComboBoxColumn();
                    column.Name = "CategoryCustom";
                    column.HeaderText = "Category";
                    column.Items.Add("Tattoo Client");
                    column.Items.Add("Piercing");
                    column.Items.Add("Merchandise");
                    column.Items.Add("Tattoo Walkin");
                    column.Items.Add("Other");

                    dgView.DataSource = itemList;
                    dgView.Columns["Category"].Visible = false;
                    dgView.Columns["MerchantId"].Visible = false;
                    column.FlatStyle = FlatStyle.Flat;
                    dgView.Columns.Add(column);
                    foreach (DataGridViewRow dr in dgView.Rows)
                    {
                        ((DataGridViewComboBoxCell)dr.Cells["CategoryCustom"]).Value = Utility.GetCategory(Convert.ToInt64(dr.Cells["Category"].Value));
                    }
                    dgView.Show();
                    btnSave.Enabled = true;
                    break;
                case "Acuity":
                    Text = "Edit Acuity Mapping";
                    IList<Appointment> appListTemp = AcuityData.GetAppointments(DateTime.Now.AddDays(-180), DateTime.Now);
                    IList<AcuityMapping> mapList = DBFunc.FetchAcuityMapping();
                    foreach (string mapping in appListTemp.Select(app => app.Type).Distinct())
                    {
                        if (mapList.Count == 0 || mapList.Count(i => i.AcuityType == mapping) == 0)
                            DBFunc.InsertAcuityMapping(new AcuityMapping { AcuityType = mapping, MerchantId = "" });
                    }
                    mapList = DBFunc.FetchAcuityMapping();
                    CloverSettings _cloverSettings = Utility.DeserializeXMLFileToObject<CloverSettings>("CloverConfig.xml");

                    column = new DataGridViewComboBoxColumn();
                    column.Name = "Merchant";
                    column.HeaderText = "Store Name";
                    column.Items.Add(new ComboBoxItem { Text = "--None--", Value = "" });
                    foreach (Merchant merchant in _cloverSettings.MerchantList)
                    {
                        column.Items.Add(new ComboBoxItem { Text = merchant.Name, Value = merchant.Id });
                    }
                    
                    column.ValueMember = "Value";
                    column.DisplayMember = "Text";
                    dgView.DataSource = mapList;
                    column.FlatStyle = FlatStyle.Flat;
                    dgView.Columns.Add(column);
                    dgView.Show();
                    dgView.Columns["MerchantId"].Visible = false;
                    foreach (DataGridViewRow dr in dgView.Rows)
                    {                        
                        ((DataGridViewComboBoxCell)dr.Cells["Merchant"]).Value = dr.Cells["MerchantId"].Value.ToString();
                    }
                    btnSave.Enabled = true;
                    break;
                case "EmployeeRates":
                    this.Text = "Edit Employee Commission Rates";
                    btnAddEmpRate.Visible = true;
                    btnDeleteEmpRate.Visible = true;
                    List<EmployeeRate> empRates = DBFunc.FetchEmployeeRates();
                    dgView.DataSource = empRates;
                    dgView.Show();
                    dgView.MultiSelect = false;
                    foreach (DataGridViewColumn col in dgView.Columns)
                    {
                        col.DefaultCellStyle.Format = "0\\%";
                    }
                    btnSave.Enabled = true;
                    break;
                case "Tender":
                    this.Text = "Edit Clover Tender Configuration for Store: " + Merchant.Name;
                    List<Tender> tenderList = DBFunc.FetchCloverTenders(Merchant.Id);
                    CloverData.MerchantId = Merchant.Id;
                    IList<Tender> cloverTenders = CloverData.FetchCloverTenders();
                    foreach (Tender tender in cloverTenders)
                    {
                        tender.MerchantId = Merchant.Id;
                        if (tenderList.Count == 0 || tenderList.Count(i => i.Id == tender.Id) == 0)
                            DBFunc.InsertCloverTender(tender);
                    }
                    tenderList = DBFunc.FetchCloverTenders(Merchant.Id);
                    column = new DataGridViewComboBoxColumn();
                    column.Name = "PayTypeCustom";
                    column.HeaderText = "Pay Type";
                    column.Items.Add("Cash");
                    column.Items.Add("Credit Card");
                    column.Items.Add("Other");

                    dgView.DataSource = tenderList;
                    dgView.Columns["PayType"].Visible = false;
                    dgView.Columns["MerchantId"].Visible = false;
                    column.FlatStyle = FlatStyle.Flat;
                    dgView.Columns.Add(column);
                    foreach (DataGridViewRow dr in dgView.Rows)
                    {
                        ((DataGridViewComboBoxCell)dr.Cells["PayTypeCustom"]).Value = Utility.GetPayType(Convert.ToInt64(dr.Cells["PayType"].Value));
                    }
                    dgView.Show();
                    btnSave.Enabled = true;
                    break;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            switch (Mode)
            {
                case "Inventory":
                    foreach (DataGridViewRow dr in dgView.Rows)
                    {
                        DBFunc.UpdateCloverInventory(new Item
                        {
                            Id = ((Item)dr.DataBoundItem).Id,
                            Category = Utility.GetCategoryId(dr.Cells["CategoryCustom"].Value.ToString()),
                            MerchantId= dr.Cells["MerchantId"].Value == null ? "" : dr.Cells["MerchantId"].Value.ToString()
                        });
                    }
                    break;
                case "Tender":
                    foreach (DataGridViewRow dr in dgView.Rows)
                    {
                        DBFunc.UpdateCloverTender(new Tender
                        {
                            Id = ((Tender)dr.DataBoundItem).Id,
                            PayType = Utility.GetPayTypeId(dr.Cells["PayTypeCustom"].Value.ToString()),
                            MerchantId= dr.Cells["MerchantId"].Value == null ? "" : dr.Cells["MerchantId"].Value.ToString(),

                        });
                    }
                    break;
                case "EmployeeRates":
                    foreach (DataGridViewRow dr in dgView.Rows)
                    {
                        DBFunc.UpdateEmployeeRates(new EmployeeRate
                        {
                            Id = ((EmployeeRate)dr.DataBoundItem).Id,
                            Name = ((EmployeeRate)dr.DataBoundItem).Name,
                            WalkInRate = Convert.ToInt64(dr.Cells["WalkInRate"].Value),
                            PersonalClientRate = Convert.ToInt64(dr.Cells["PersonalClientRate"].Value),
                            MerchandiseRate = Convert.ToInt64(dr.Cells["MerchandiseRate"].Value),
                            PiercingRate = Convert.ToInt64(dr.Cells["PiercingRate"].Value),
                            AcuityRate = Convert.ToInt64(dr.Cells["AcuityRate"].Value)
                        });
                    }
                    break;
                case "Acuity":
                    foreach (DataGridViewRow dr in dgView.Rows)
                    {
                        DBFunc.UpdateAcuityMapping(new AcuityMapping
                        {
                            Id = ((AcuityMapping)dr.DataBoundItem).Id,
                            MerchantId = dr.Cells["Merchant"].Value == null ? "" : dr.Cells["Merchant"].Value.ToString(),
                            AcuityType = dr.Cells["AcuityType"].ToString()
                        });
                    }
                    break;
            }
            MessageBox.Show("Update Completed");
            Close();
        }

        private void btnAddEmpRate_Click(object sender, EventArgs e)
        {
            DBFunc.InsertEmployeeRate(new EmployeeRate
            {
                Id = "Id" + DateTime.Now.ToString("ddMMyyyyss"),
                Name = "",
                AcuityRate = 50,
                MerchandiseRate = 50,
                PersonalClientRate = 50,
                PiercingRate = 50,
                WalkInRate = 50
            });
            List<EmployeeRate> empRates = DBFunc.FetchEmployeeRates();
            dgView.DataSource = empRates;




        }
        private static string ReturnTimeStreamReader(string path, string symbol)
        {
            List<String[]> collection = new List<String[]>();
            using (StreamReader sr = new StreamReader(path))
            {
                string data = sr.ReadLine();
                int count = 0;
                while ((data = sr.ReadLine()) != null)
                {
                    collection.Add(data.Split(','));
                }
            }
            string time = "00:00";

            if (collection.Where(c => c[0] == symbol).Any())
                time = collection.Where(c => c[0] == symbol).First()[1].ToString();
            return time;

        }
        private void btnDeleteEmpRate_Click(object sender, EventArgs e)
        {
            DataGridViewRow dr = dgView.SelectedRows[0];
            DBFunc.DeleteEmployeeRate(new EmployeeRate { Id = ((EmployeeRate)dr.DataBoundItem).Id });
            List<EmployeeRate> empRates = DBFunc.FetchEmployeeRates();
            dgView.DataSource = empRates;
        }
    }
    public class ComboBoxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }

}

