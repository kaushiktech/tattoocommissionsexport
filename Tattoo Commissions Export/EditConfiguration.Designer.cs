﻿namespace Tattoo_Commissions_Export
{
    partial class frmEditConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditConfiguration));
            this.dgView = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddEmpRate = new System.Windows.Forms.Button();
            this.btnDeleteEmpRate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).BeginInit();
            this.SuspendLayout();
            // 
            // dgView
            // 
            this.dgView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgView.Location = new System.Drawing.Point(12, 68);
            this.dgView.Name = "dgView";
            this.dgView.RowTemplate.Height = 28;
            this.dgView.Size = new System.Drawing.Size(943, 335);
            this.dgView.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(780, 409);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 35);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(880, 409);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 35);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddEmpRate
            // 
            this.btnAddEmpRate.Location = new System.Drawing.Point(12, 409);
            this.btnAddEmpRate.Name = "btnAddEmpRate";
            this.btnAddEmpRate.Size = new System.Drawing.Size(137, 35);
            this.btnAddEmpRate.TabIndex = 3;
            this.btnAddEmpRate.Text = "Add Employee";
            this.btnAddEmpRate.UseVisualStyleBackColor = true;
            this.btnAddEmpRate.Visible = false;
            this.btnAddEmpRate.Click += new System.EventHandler(this.btnAddEmpRate_Click);
            // 
            // btnDeleteEmpRate
            // 
            this.btnDeleteEmpRate.Location = new System.Drawing.Point(155, 409);
            this.btnDeleteEmpRate.Name = "btnDeleteEmpRate";
            this.btnDeleteEmpRate.Size = new System.Drawing.Size(154, 35);
            this.btnDeleteEmpRate.TabIndex = 4;
            this.btnDeleteEmpRate.Text = "Delete Employee";
            this.btnDeleteEmpRate.UseVisualStyleBackColor = true;
            this.btnDeleteEmpRate.Visible = false;
            this.btnDeleteEmpRate.Click += new System.EventHandler(this.btnDeleteEmpRate_Click);
            // 
            // frmEditConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 494);
            this.Controls.Add(this.btnDeleteEmpRate);
            this.Controls.Add(this.btnAddEmpRate);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEditConfiguration";
            this.Text = "Edit Clover Inventory";
            this.Load += new System.EventHandler(this.EditConfiguration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgView;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAddEmpRate;
        private System.Windows.Forms.Button btnDeleteEmpRate;
    }
}