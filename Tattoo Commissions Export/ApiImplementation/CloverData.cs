﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using Tattoo_Commissions_Export.Data;
using Tattoo_Commissions_Export.Data.ApiObjects;
using Tattoo_Commissions_Export.Data.DataObjects;

namespace Tattoo_Commissions_Export.ApiImplementation
{
    public static class CloverData
    {
        public static CloverSettings Settings;        
        public static string MerchantId;
        private static IEnumerable<ResultType> FetchCloverObjects<ResultType>(string resource, DateTimeOffset? startDate, DateTimeOffset? endDate, bool filterUrl) where ResultType : CloverObject
        {
            IDictionary<string, ResultType> resultItems = new Dictionary<string, ResultType>();
            bool done = false;
            long lastFetch = 0;
            if (startDate.HasValue)
            {
                lastFetch = startDate.Value.ToUnixTimeMilliseconds();
            }
            while (!done)
            {
                string accessToken = Settings.MerchantList.First(m=>m.Id==MerchantId).ApiSecret;
                string requestString = string.Empty;
                if (filterUrl)
                    requestString = string.Format(Settings.FilterApiUrl, MerchantId, resource, accessToken, lastFetch, Settings.Limit);
                else
                    requestString = string.Format(Settings.ApiUrl, MerchantId, resource, accessToken);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestString);
                string json = GetJson(request);

                CloverWebResponse<ResultType> response = JsonConvert.DeserializeObject<CloverWebResponse<ResultType>>(json);

                foreach (ResultType item in response.elements)
                {
                    resultItems[item.Id] = item;
                }
                if (response.elements.Count() < Settings.Limit)
                {
                    done = true;
                }
                else
                {
                    lastFetch = response.elements.Max(item => item.CreatedTime - 1);
                    if (lastFetch >= endDate.Value.ToUnixTimeMilliseconds())
                        done = true;
                }
            }
            return resultItems.Values;
        }
        private static ResponseType TranslateObject<ResponseType>(Func<ResponseType> translateFunction)
        {
            try
            {
                return translateFunction();
            }
            catch (Exception exception)
            {
                throw;
            }
        }
        public static IList<Item> FetchCloverInventory()
        {
            IList<Item> resultObjects = new List<Item>();
            foreach (CloverItem item in FetchCloverObjects<CloverItem>("items", null, null,false))
            {
                resultObjects.Add(
                    TranslateObject(() =>
                    {
                        return new Item()
                        {
                            Id = item.Id,
                            PriceType = item.PriceType,
                            Name = item.Name,
                            Price = Convert.ToDecimal(item.Price) / 100,
                        };
                    }));
            }
            return resultObjects;

        }
        public static IList<Tender> FetchCloverTenders()
        {
            IList<Tender> resultObjects = new List<Tender>();
            foreach (CloverTender item in FetchCloverObjects<CloverTender>("tenders", null, null, false))
            {
                resultObjects.Add(
                    TranslateObject(() =>
                    {
                        return new Tender()
                        {
                            Id = item.Id,
                            Enabled = item.Enabled,
                            Label = item.Label,
                            OpensCashDrawer = item.OpensCashDrawer,
                            Visible = item.Visible
                        };
                    }));
            }
            return resultObjects;

        }
        public static IList<Employee> FetchCloverEmployees()
        {
            IList<Employee> resultObjects = new List<Employee>();
            foreach (CloverEmployee emp in FetchCloverObjects<CloverEmployee>("employees", null, null, false))
            {
                resultObjects.Add(
                    TranslateObject(() =>
                    {
                        return new Employee()
                        {
                            Id = emp.Id,
                            Name = emp.Name,
                            Role = emp.Role,
                            IsOwner = emp.IsOwner,
                            Nickname = emp.Nickname,
                            Pin = emp.Pin
                        };
                    }));
            }
            return resultObjects;
        }
        private static IList<LineItem> FetchCloverLineItems(DateTimeOffset? startDate,DateTimeOffset? endDate)
        {
            IList<LineItem> resultObjects = new List<LineItem>();
            foreach (CloverLineItem lineItem in FetchCloverObjects<CloverLineItem>("line_items", startDate, endDate, true))
            {

                resultObjects.Add(
                    TranslateObject(() =>
                    {
                        return new LineItem()
                        {
                            Id = lineItem.Id,
                            CreatedTime = LongToDateTimeOffsetNullable(lineItem.CreatedTime).Value,
                            OrderId = lineItem.Order.Id,
                            Name = lineItem.Name,
                            Price = lineItem.Price.HasValue ? (decimal?)lineItem.Price / 100 : null,
                            UnitQuantity = lineItem.UnitQuantity,
                            IsRefunded = lineItem.IsRefunded,
                            QuantitySold = lineItem.QuantitySold,
                            ItemId = lineItem.Item?.Id
                        };
                    }));
            }

            return resultObjects;

        }
        private static IList<Order> FetchCloverOrders(DateTimeOffset? startDate,DateTimeOffset? endDate)
        {
            IList<Order> resultObjects = new List<Order>();
            foreach (CloverOrder order in FetchCloverObjects<CloverOrder>("orders", startDate, endDate, true))
            {
                resultObjects.Add(
                    TranslateObject(() =>
                    {
                        return new Order()
                        {
                            Id = order.Id,
                            CreatedTime = LongToDateTimeOffsetNullable(order.CreatedTime).Value,
                            Currency = order.Currency,
                            EmployeeId = order.Employee?.Id,
                            Total = order.Total.HasValue ? (decimal?)order.Total / 100 : null,
                            OrderStatusId = GetOrderStatusId(order.State)
                        };
                    }));
            }
            return resultObjects;

        }
        private static IList<Payment> FetchCloverPayments(DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            IList<Payment> resultObjects = new List<Payment>();
            foreach (CloverPayment payment in FetchCloverObjects<CloverPayment>("payments", startDate, endDate, true))
            {
                resultObjects.Add(
                    TranslateObject(() =>
                    {
                        return new Payment()
                        {
                            Id = payment.Id,
                            OrderId = payment.Order != null ? payment.Order.Id : "",
                            CreatedTime = LongToDateTimeOffsetNullable(payment.CreatedTime).Value,
                            TenderId = payment.Tender != null ? payment.Tender.Id : ""
                        };
                    }));
            }
            return resultObjects;

        }
        private static int? GetOrderStatusId(string orderState)
        {
            CloverOrderStatus result;
            if (Enum.TryParse(orderState, true, out result))
            {
                return (int)result;
            }
            else
            {
                return null;
            }
        }
        private static DateTimeOffset? LongToDateTimeOffsetNullable(long? time)
        {
            if (!time.HasValue || time.Value == 0)
            {
                return null;
            }
            return DateTimeOffset.FromUnixTimeMilliseconds(time.Value);
        }
        private static string GetJson(HttpWebRequest request)
        {
            request.Method = "GET";
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string responseString;
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    responseString = reader.ReadToEnd();
                    responseStream.Close();
                }
                return responseString;
            }
            catch (Exception exception)
            {
                //Context.Logger.LogError(exception, "{0}.{1}: error reading Clover Service. {2}:{3}:{4}", Id, nameof(GetJson), exception.GetType(), exception.Message, exception.StackTrace);
                throw;
            }
        }
        public static IList<Order> GetOrders(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            IList<Order> orderList = FetchCloverOrders(startDate, endDate);
            if (orderList.Any(ord => ord.CreatedTime > startDate && ord.CreatedTime <= endDate))
                orderList = orderList.Where(ord => ord.CreatedTime > startDate && ord.CreatedTime <= endDate).ToList();
            return orderList;
        }
        public static IList<Payment> GetPayments(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            IList<Payment> paymentList = FetchCloverPayments(startDate, endDate);
            if (paymentList.Any(ord => ord.CreatedTime > startDate && ord.CreatedTime <= endDate))
                paymentList = paymentList.Where(ord => ord.CreatedTime > startDate && ord.CreatedTime <= endDate).ToList();
            return paymentList;
        }
        public static IList<LineItem> GetLineItem(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            IList<LineItem> lineItemList = FetchCloverLineItems(startDate,endDate);
            if (lineItemList.Any(ord => ord.CreatedTime > startDate && ord.CreatedTime <= endDate))
                lineItemList = lineItemList.Where(ord => ord.CreatedTime > startDate && ord.CreatedTime <= endDate).ToList();
            return lineItemList;
        }
    }
    public enum CloverOrderStatus
    {
        Locked = 1,
        Open = 2
    }
}
