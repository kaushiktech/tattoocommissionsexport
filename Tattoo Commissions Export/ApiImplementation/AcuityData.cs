﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Tattoo_Commissions_Export.Data;
using Tattoo_Commissions_Export.Data.ApiObjects;
using Tattoo_Commissions_Export.Data.DataObjects;

namespace Tattoo_Commissions_Export.ApiImplementation
{
    public static class AcuityData
    {
        public static AcuitySettings Settings;
        private static IList<ResultType> FetchAcuityObjects<ResultType>(string resource, DateTime startDate,DateTime endDate) where ResultType : AcuityObject
        {

            List<ResultType> response = new List<ResultType>();
            string requestString = string.Empty;
            requestString = string.Format(Settings.ApiUrl, resource, Settings.Limit, startDate.ToString("MMMM dd, yyyy"), endDate.ToString("MMMM dd, yyyy"));
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestString);
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(Settings.Users[0].Id + ":" + Settings.Users[0].ApiKey));
            request.Headers.Add("Authorization", "Basic " + encoded);
            string json = GetJson(request);
            response = JsonConvert.DeserializeObject<List<ResultType>>(json);
            return response;
        }
        private static ResponseType TranslateObject<ResponseType>(Func<ResponseType> translateFunction)
        {
            try
            {
                return translateFunction();
            }
            catch (Exception exception)
            {
                throw;
            }
        }
        private static string GetJson(HttpWebRequest request)
        {
            request.Method = "GET";
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string responseString;
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    responseString = reader.ReadToEnd();
                    responseStream.Close();
                }
                return responseString;
            }
            catch (Exception exception)
            {
                //Context.Logger.LogError(exception, "{0}.{1}: error reading Clover Service. {2}:{3}:{4}", Id, nameof(GetJson), exception.GetType(), exception.Message, exception.StackTrace);
                throw;
            }
        }
        private static IList<Appointment> FetchAcuityAppointments(DateTime startDate,DateTime endDate)
        {
            IList<Appointment> resultObjects = new List<Appointment>();
            foreach (AcuityAppointment app in FetchAcuityObjects<AcuityAppointment>("appointments", startDate,endDate))
            {
                resultObjects.Add(
                    TranslateObject(() =>
                    {
                        return new Appointment()
                        {
                            Id = app.Id,
                            CreatedTime = app.CreatedTime,
                            ArtistName = app.ArtistName,
                            Category = app.Category,
                            Paid = app.Paid,
                            Type = app.Type,
                            Price=app.Price,
                            Date=app.Date
                        };
                    }));
            }
            return resultObjects;

        }
        public static IList<Appointment> GetAppointments(DateTime startDate, DateTime endDate)
        {
            List<Appointment> app = new List<Appointment>();
            int count = Settings.Limit;
            while ((app.Count == 0 || app.OrderBy(ord => ord.CreatedTime).Last().CreatedTime < endDate) && count == 1000)
            {
                DateTime lastDatePulled;
                if (app.Count != 0)
                    lastDatePulled = app.OrderBy(ord => ord.CreatedTime).Last().CreatedTime;
                else
                    lastDatePulled = startDate;
                IList<Appointment> appList = FetchAcuityAppointments(lastDatePulled,endDate);
                app.AddRange(appList);
                count = app.Count;
            }
            app = app.Distinct().ToList();
            return app;
        }
    }
}
