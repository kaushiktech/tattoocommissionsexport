﻿namespace Tattoo_Commissions_Export
{
    partial class frmTattoo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTattoo));
            this.dtStartTime = new System.Windows.Forms.DateTimePicker();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dtEndTime = new System.Windows.Forms.DateTimePicker();
            this.btnLoadData = new System.Windows.Forms.Button();
            this.dgView = new System.Windows.Forms.DataGridView();
            this.btnEditInventory = new System.Windows.Forms.Button();
            this.btnEmployeeRates = new System.Windows.Forms.Button();
            this.btnEditTenders = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnExportDetail = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).BeginInit();
            this.SuspendLayout();
            // 
            // dtStartTime
            // 
            this.dtStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtStartTime.Location = new System.Drawing.Point(114, 71);
            this.dtStartTime.Name = "dtStartTime";
            this.dtStartTime.Size = new System.Drawing.Size(200, 26);
            this.dtStartTime.TabIndex = 0;
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(25, 76);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(83, 20);
            this.lblStartDate.TabIndex = 1;
            this.lblStartDate.Text = "Start Date";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(342, 76);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(77, 20);
            this.lblEndDate.TabIndex = 2;
            this.lblEndDate.Text = "End Date";
            // 
            // dtEndTime
            // 
            this.dtEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtEndTime.Location = new System.Drawing.Point(436, 70);
            this.dtEndTime.Name = "dtEndTime";
            this.dtEndTime.Size = new System.Drawing.Size(200, 26);
            this.dtEndTime.TabIndex = 3;
            // 
            // btnLoadData
            // 
            this.btnLoadData.Location = new System.Drawing.Point(334, 244);
            this.btnLoadData.Name = "btnLoadData";
            this.btnLoadData.Size = new System.Drawing.Size(109, 41);
            this.btnLoadData.TabIndex = 4;
            this.btnLoadData.Text = "Search";
            this.btnLoadData.UseVisualStyleBackColor = true;
            this.btnLoadData.Click += new System.EventHandler(this.btnLoadData_Click);
            // 
            // dgView
            // 
            this.dgView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgView.Location = new System.Drawing.Point(12, 291);
            this.dgView.Name = "dgView";
            this.dgView.RowTemplate.Height = 28;
            this.dgView.Size = new System.Drawing.Size(847, 408);
            this.dgView.TabIndex = 5;
            // 
            // btnEditInventory
            // 
            this.btnEditInventory.Location = new System.Drawing.Point(48, 148);
            this.btnEditInventory.Name = "btnEditInventory";
            this.btnEditInventory.Size = new System.Drawing.Size(145, 35);
            this.btnEditInventory.TabIndex = 6;
            this.btnEditInventory.Text = "Edit Inventory";
            this.btnEditInventory.UseVisualStyleBackColor = true;
            this.btnEditInventory.Click += new System.EventHandler(this.btnEditInventory_Click);
            // 
            // btnEmployeeRates
            // 
            this.btnEmployeeRates.Location = new System.Drawing.Point(350, 148);
            this.btnEmployeeRates.Name = "btnEmployeeRates";
            this.btnEmployeeRates.Size = new System.Drawing.Size(209, 35);
            this.btnEmployeeRates.TabIndex = 7;
            this.btnEmployeeRates.Text = "Edit Employee Rates";
            this.btnEmployeeRates.UseVisualStyleBackColor = true;
            this.btnEmployeeRates.Click += new System.EventHandler(this.btnEmployeeRates_Click);
            // 
            // btnEditTenders
            // 
            this.btnEditTenders.Location = new System.Drawing.Point(199, 148);
            this.btnEditTenders.Name = "btnEditTenders";
            this.btnEditTenders.Size = new System.Drawing.Size(145, 35);
            this.btnEditTenders.TabIndex = 8;
            this.btnEditTenders.Text = "Edit Tenders";
            this.btnEditTenders.UseVisualStyleBackColor = true;
            this.btnEditTenders.Click += new System.EventHandler(this.btnEditTenders_Click);
            // 
            // btnExport
            // 
            this.btnExport.Enabled = false;
            this.btnExport.Location = new System.Drawing.Point(582, 244);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(115, 41);
            this.btnExport.TabIndex = 9;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnExportDetail
            // 
            this.btnExportDetail.Enabled = false;
            this.btnExportDetail.Location = new System.Drawing.Point(703, 244);
            this.btnExportDetail.Name = "btnExportDetail";
            this.btnExportDetail.Size = new System.Drawing.Size(155, 41);
            this.btnExportDetail.TabIndex = 10;
            this.btnExportDetail.Text = "Export Detailed";
            this.btnExportDetail.UseVisualStyleBackColor = true;
            this.btnExportDetail.Click += new System.EventHandler(this.btnExportDetail_Click);
            // 
            // frmTattoo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 711);
            this.Controls.Add(this.btnExportDetail);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnEditTenders);
            this.Controls.Add(this.btnEmployeeRates);
            this.Controls.Add(this.btnEditInventory);
            this.Controls.Add(this.dgView);
            this.Controls.Add(this.btnLoadData);
            this.Controls.Add(this.dtEndTime);
            this.Controls.Add(this.lblEndDate);
            this.Controls.Add(this.lblStartDate);
            this.Controls.Add(this.dtStartTime);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmTattoo";
            this.Text = "Tattoo Commissions Export";
            this.Load += new System.EventHandler(this.frmTattoo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtStartTime;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtEndTime;
        private System.Windows.Forms.Button btnLoadData;
        private System.Windows.Forms.DataGridView dgView;
        private System.Windows.Forms.Button btnEditInventory;
        private System.Windows.Forms.Button btnEmployeeRates;
        private System.Windows.Forms.Button btnEditTenders;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnExportDetail;
    }
}

