﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Tattoo_Commissions_Export
{
    public static class Utility
    {
        public static T DeserializeXMLFileToObject<T>(string XmlFilename)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(XmlFilename)) return default(T);

            try
            {
                StreamReader xmlStream = new StreamReader(XmlFilename);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch (Exception ex)
            {
                //ExceptionLogger.WriteExceptionToConsole(ex, DateTime.Now);
            }
            return returnObject;
        }
        public static string GetCategory(long category)
        {
            switch (category)
            {
                case 1:
                    return "Tattoo Client";
                case 2:
                    return "Piercing";
                case 3:
                    return "Merchandise";
                case 4:
                    return "Acuity";
                case 5:
                    return "Tattoo Walkin";
                case 0:
                    return "Other";
            }
            return "Other";
        }
        public static int GetCategoryId(string category)
        {
            switch (category)
            {
                case "Tattoo Client":
                    return 1;
                case "Piercing":
                    return 2;
                case "Merchandise":
                    return 3;                
                case "Acuity":
                    return 4;
                case "Tattoo Walkin":
                    return 5;
                case "Other":
                    return 0;
            }
            return 0;
        }
        public static void Export2CSV(DataTable dt, string path)
        {
            StringBuilder sb = new StringBuilder();
            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }
            File.WriteAllText(path, sb.ToString());
        }
        public static void Convert<T>(this DataColumn column, Func<object, T> conversion)
        {
            foreach (DataRow row in column.Table.Rows)
            {
                row[column] = conversion(row[column]);
            }
        }
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        public static string GetPayType(long payTypeId)
        {
            switch (payTypeId)
            {
                case 1:
                    return "Cash";
                case 2:
                    return "Credit Card";
                case 0:
                    return "Other";
            }
            return "Other";
        }
        public static int GetPayTypeId(string payType)
        {
            switch (payType)
            {
                case "Cash":
                    return 1;
                case "Credit Card":
                    return 2;
                case "Other":
                    return 0;
            }
            return 0;
        }
    }
}
