﻿namespace Tattoo_Commissions_Export
{
    partial class TattooFormV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TattooFormV2));
            this.storeTabControl = new System.Windows.Forms.TabControl();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGlobalExport = new System.Windows.Forms.Button();
            this.btnGlobalExportDetails = new System.Windows.Forms.Button();
            this.btnAcuityMapping = new System.Windows.Forms.Button();
            this.btnEditRates = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // storeTabControl
            // 
            this.storeTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.storeTabControl.Location = new System.Drawing.Point(1, 53);
            this.storeTabControl.Name = "storeTabControl";
            this.storeTabControl.SelectedIndex = 0;
            this.storeTabControl.Size = new System.Drawing.Size(971, 979);
            this.storeTabControl.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(280, 37);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tattoo Form Export";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnGlobalExport
            // 
            this.btnGlobalExport.Enabled = false;
            this.btnGlobalExport.Location = new System.Drawing.Point(663, 13);
            this.btnGlobalExport.Name = "btnGlobalExport";
            this.btnGlobalExport.Size = new System.Drawing.Size(122, 34);
            this.btnGlobalExport.TabIndex = 2;
            this.btnGlobalExport.Text = "Full Export";
            this.btnGlobalExport.UseVisualStyleBackColor = true;
            this.btnGlobalExport.Click += new System.EventHandler(this.btnGlobalExport_Click);
            // 
            // btnGlobalExportDetails
            // 
            this.btnGlobalExportDetails.Enabled = false;
            this.btnGlobalExportDetails.Location = new System.Drawing.Point(791, 12);
            this.btnGlobalExportDetails.Name = "btnGlobalExportDetails";
            this.btnGlobalExportDetails.Size = new System.Drawing.Size(169, 34);
            this.btnGlobalExportDetails.TabIndex = 3;
            this.btnGlobalExportDetails.Text = "Full Export Details";
            this.btnGlobalExportDetails.UseVisualStyleBackColor = true;
            this.btnGlobalExportDetails.Click += new System.EventHandler(this.btnGlobalExportDetails_Click);
            // 
            // btnAcuityMapping
            // 
            this.btnAcuityMapping.Location = new System.Drawing.Point(484, 12);
            this.btnAcuityMapping.Name = "btnAcuityMapping";
            this.btnAcuityMapping.Size = new System.Drawing.Size(173, 34);
            this.btnAcuityMapping.TabIndex = 4;
            this.btnAcuityMapping.Text = "Acuity Mapping";
            this.btnAcuityMapping.UseVisualStyleBackColor = true;
            this.btnAcuityMapping.Click += new System.EventHandler(this.btnAcuityMapping_Click);
            // 
            // btnEditRates
            // 
            this.btnEditRates.Location = new System.Drawing.Point(299, 12);
            this.btnEditRates.Name = "btnEditRates";
            this.btnEditRates.Size = new System.Drawing.Size(179, 34);
            this.btnEditRates.TabIndex = 5;
            this.btnEditRates.Text = "Edit Employee Rates";
            this.btnEditRates.UseVisualStyleBackColor = true;
            this.btnEditRates.Click += new System.EventHandler(this.btnEditRates_Click);
            // 
            // TattooFormV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 1044);
            this.Controls.Add(this.btnEditRates);
            this.Controls.Add(this.btnAcuityMapping);
            this.Controls.Add(this.btnGlobalExportDetails);
            this.Controls.Add(this.btnGlobalExport);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.storeTabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TattooFormV2";
            this.Text = "Tattoo Form Export";
            this.Load += new System.EventHandler(this.TattooFormV2_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl storeTabControl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGlobalExport;
        private System.Windows.Forms.Button btnGlobalExportDetails;
        private System.Windows.Forms.Button btnAcuityMapping;
        private System.Windows.Forms.Button btnEditRates;
    }
}