﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Tattoo_Commissions_Export.Data.DataObjects;

namespace Tattoo_Commissions_Export.DBImplementation
{
    public static class DBFunc
    {
        static string connString = "Data Source=TattooData.db;Version=3;New=True;Compress=True;";
        public static void InsertCloverItem(Item item)
        {

            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                string InsertSql = @"INSERT INTO tbl_CloverInventory 
                        (Id,MerchantId, Name, Price, PriceType, Category) 
                        VALUES ($id,$merchantid, $name, $price, $priceType, $category)";
                using (SQLiteCommand cmd = new SQLiteCommand(InsertSql, sql))
                {
                    cmd.Parameters.Add("$id", DbType.String).Value = item.Id;
                    cmd.Parameters.Add("$name", DbType.String).Value = item.Name;
                    cmd.Parameters.Add("$price", DbType.Decimal).Value = item.Price;
                    cmd.Parameters.Add("$priceType", DbType.String).Value = item.PriceType;
                    cmd.Parameters.Add("$category", DbType.Int32).Value = 0;
                    cmd.Parameters.Add("$merchantid", DbType.String).Value = item.MerchantId;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void InsertCloverTender(Tender item)
        {

            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                string InsertSql = @"INSERT INTO tbl_CloverTenders
                        (Id,MerchantId, Label, OpensCashDrawer, Enabled, Visible) 
                        VALUES ($id,$merchantid, $label, $openscashdrawer, $enabled, $visible)";
                using (SQLiteCommand cmd = new SQLiteCommand(InsertSql, sql))
                {
                    cmd.Parameters.Add("$id", DbType.String).Value = item.Id;
                    cmd.Parameters.Add("$merchantid", DbType.String).Value = item.MerchantId;
                    cmd.Parameters.Add("$label", DbType.String).Value = item.Label;
                    cmd.Parameters.Add("$openscashdrawer", DbType.Boolean).Value = item.OpensCashDrawer;
                    cmd.Parameters.Add("$enabled", DbType.Boolean).Value = item.Enabled;
                    cmd.Parameters.Add("$visible", DbType.Boolean).Value = item.Visible;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void InsertAcuityMapping(AcuityMapping item)
        {
            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                string InsertSql = @"INSERT INTO tbl_AcuityMapping
                        (MerchantId, AcuityType) 
                        VALUES ($merchantid, $type)";
                using (SQLiteCommand cmd = new SQLiteCommand(InsertSql, sql))
                {
                    cmd.Parameters.Add("$merchantid", DbType.String).Value = item.MerchantId;
                    cmd.Parameters.Add("$type", DbType.String).Value = item.AcuityType;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static List<AcuityMapping> FetchAcuityMapping(string merchantId = "")
        {
            List<AcuityMapping> items = new List<AcuityMapping>();
            SQLiteConnection sql;
            using (sql = new SQLiteConnection(connString))
            {
                sql.Open();
                using (SQLiteCommand cmd = sql.CreateCommand())
                {
                    if (string.IsNullOrEmpty(merchantId))
                        cmd.CommandText = "SELECT * FROM tbl_AcuityMapping";
                    else
                        cmd.CommandText = "SELECT * FROM tbl_AcuityMapping WHERE MerchantId='" + merchantId + "'";
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    adapter.Dispose();
                    if (dt.Rows.Count > 0)
                        items = ConvertDataTable<AcuityMapping>(dt);
                }
            }
            return items;
        }
        public static List<Item> FetchCloverInventory(string merchantId)
        {
            List<Item> items = new List<Item>();
            SQLiteConnection sql;
            using (sql = new SQLiteConnection(connString))
            {
                sql.Open();
                using (SQLiteCommand cmd = sql.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM tbl_CloverInventory WHERE MerchantId='" + merchantId + "'";
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    adapter.Dispose();
                    if (dt.Rows.Count > 0)
                        items = ConvertDataTable<Item>(dt);
                }
            }
            return items;
        }
        public static List<Tender> FetchCloverTenders(string merchantId)
        {
            List<Tender> items = new List<Tender>();
            SQLiteConnection sql;
            using (sql = new SQLiteConnection(connString))
            {
                sql.Open();
                using (SQLiteCommand cmd = sql.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM tbl_CloverTenders where MerchantId='" + merchantId + "'";
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    adapter.Dispose();
                    foreach (DataRow dr in dt.Rows)
                    {
                        items.Add(new Tender
                        {
                            Id = dr["Id"].ToString(),
                            Enabled = dr["Enabled"].ToString() == "1" ? true : false,
                            Visible = dr["Visible"].ToString() == "1" ? true : false,
                            OpensCashDrawer = dr["OpensCashDrawer"].ToString() == "1" ? true : false,
                            Label = dr["Label"].ToString(),
                            MerchantId = dr["MerchantId"].ToString(),
                            PayType = Convert.ToInt16(dr["PayType"])
                        });
                    }
                }
            }
            return items;
        }
        public static void UpdateCloverInventory(Item item)
        {

            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                string query = "UPDATE tbl_CloverInventory SET ";
                query += " Category=" + item.Category;
                query += " Where Id='" + item.Id + "'";
                using (SQLiteCommand cmd = new SQLiteCommand(query, sql))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void UpdateAcuityMapping(AcuityMapping item)
        {

            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                string query = "UPDATE tbl_AcuityMapping SET ";
                query += "MerchantId='" + item.MerchantId + "'";
                query += " Where Id=" + item.Id;
                using (SQLiteCommand cmd = new SQLiteCommand(query, sql))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void UpdateCloverTender(Tender item)
        {
            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                string query = "UPDATE tbl_CloverTenders SET ";
                query += " PayType=" + item.PayType;
                query += " Where Id='" + item.Id + "'";
                using (SQLiteCommand cmd = new SQLiteCommand(query, sql))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void UpdateEmployeeRates(EmployeeRate item)
        {
            List<Item> items = new List<Item>();
            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                string query = "UPDATE tbl_EmployeeRate SET ";
                query += " Name='" + item.Name + "',";
                query += " WalkInRate=" + item.WalkInRate + ",";
                query += " PersonalClientRate=" + item.PersonalClientRate + ",";
                query += " MerchandiseRate=" + item.MerchandiseRate + ",";
                query += " PiercingRate=" + item.PiercingRate + ",";
                query += " AcuityRate=" + item.AcuityRate;
                query += " Where Id='" + item.Id + "'";
                using (SQLiteCommand cmd = new SQLiteCommand(query, sql))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void InsertEmployeeRate(EmployeeRate item)
        {
            List<Item> items = new List<Item>();
            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                string InsertSql = @"INSERT INTO tbl_EmployeeRate
                        (Id,Name, WalkinRate, PersonalClientRate, MerchandiseRate, PiercingRate,AcuityRate) 
                        VALUES ($Id,$Name, $WalkinRate, $PersonalClientRate, $MerchandiseRate, $PiercingRate,
                        $AcuityRate)";
                using (SQLiteCommand cmd = new SQLiteCommand(InsertSql, sql))
                {
                    cmd.Parameters.Add("$Id", DbType.String).Value = item.Id;
                    cmd.Parameters.Add("$Name", DbType.String).Value = item.Name;
                    cmd.Parameters.Add("$WalkinRate", DbType.Decimal).Value = item.WalkInRate;
                    cmd.Parameters.Add("$PersonalClientRate", DbType.Decimal).Value = item.PersonalClientRate;
                    cmd.Parameters.Add("$MerchandiseRate", DbType.Decimal).Value = item.MerchandiseRate;
                    cmd.Parameters.Add("$PiercingRate", DbType.Decimal).Value = item.PiercingRate;
                    cmd.Parameters.Add("$AcuityRate", DbType.Decimal).Value = item.AcuityRate;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void DeleteEmployeeRate(EmployeeRate item)
        {
            List<Item> items = new List<Item>();
            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                string DeleteSql = @"DELETE FROM tbl_EmployeeRate WHERE Id=$Id";
                using (SQLiteCommand cmd = new SQLiteCommand(DeleteSql, sql))
                {
                    cmd.Parameters.Add("$Id", DbType.String).Value = item.Id;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static List<EmployeeRate> FetchEmployeeRates()
        {
            List<EmployeeRate> items = new List<EmployeeRate>();
            using (SQLiteConnection sql = new SQLiteConnection(connString))
            {
                sql.Open();
                using (SQLiteCommand cmd = sql.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM tbl_EmployeeRate";
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    if (dt.Rows.Count > 0)
                        items = ConvertDataTable<EmployeeRate>(dt);
                }
            }
            return items;
        }
        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
