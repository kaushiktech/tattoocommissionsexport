﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data
{
    public class Model<IdType>
    {
        public Model()
        {
            IsNew = true;
        }

        /// <summary>
        /// Key value that uniquely identifies the object
        /// </summary>
        [ScaffoldColumn(false)]
        public IdType Id { get; set; }
        /// <summary>
        /// Indicates the object has not yet been persisted
        /// </summary>
        public bool IsNew { get; set; }
        /// <summary>
        /// Indicates the object should be deleted when 
        /// save is called on it, or its parent
        /// </summary>
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Indicates the object has been modified in some way 
        /// and should be updated when save it called
        /// </summary>
        public bool IsDirty { get; set; }

        [ScaffoldColumn(false)]
        /// <summary>
        /// The object's version in bytes. Used for concurrency control
        /// </summary>
        public byte[] Version { get; set; }
        /// <summary>
        /// Base64-encoded version to be passed to client
        /// so it can be handed back for concurrency control
        /// </summary>
        [ScaffoldColumn(false)]
        public string VersionString
        {
            get
            {
                if (Version != null)
                    return Convert.ToBase64String(Version);
                return string.Empty;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    Version = null;
                else
                    Version = Convert.FromBase64String(value);
            }
        }
    }
}
