﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.ApiObjects
{
    [DataContract]
    public class AcuityAppointment : AcuityObject
    {
        [DataMember(Name = "price", EmitDefaultValue = false)]
        public decimal Price { get; set; }
        [DataMember(Name = "calendar", EmitDefaultValue = false)]
        public string ArtistName { get; set; }
        [DataMember(Name = "paid", EmitDefaultValue = false)]
        public string Paid { get; set; }
        [DataMember(Name = "type", EmitDefaultValue = false)]
        public string Type { get; set; }
        [DataMember(Name = "category", EmitDefaultValue = false)]
        public string Category { get; set; }
        [DataMember(Name = "date", EmitDefaultValue = false)]
        public DateTime Date { get; set; }
    }
}
