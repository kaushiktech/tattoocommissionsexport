﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Tattoo_Commissions_Export.Data.ApiObjects;

namespace Tattoo_Commissions_Export.Data
{
    [DataContract]
    public class CloverLineItem : CloverObject
    {
        [DataMember(Name = "orderRef", EmitDefaultValue = false)]
        public Reference Order { get; set; }
        //[DataMember(Name = "item", EmitDefaultValue = false)]
        //public Item Item { get; set; }
        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }
        [DataMember(Name = "price", EmitDefaultValue = false)]
        public long? Price { get; set; }
        [DataMember(Name = "unitQty", EmitDefaultValue = false)]
        public int? UnitQuantity { get; set; }
        [DataMember(Name = "unitName", EmitDefaultValue = false)]
        public string UnitName { get; set; }
        public bool? IsExchanged { get; set; }
        [DataMember(Name = "refunded", EmitDefaultValue = false)]
        public bool? IsRefunded { get; set; }        
        [DataMember(Name = "quantitySold", EmitDefaultValue = false)]
        public double? QuantitySold { get; set; }
        [DataMember(Name = "item", EmitDefaultValue = false)]
        public CloverItem Item { get; set; }
    }
}
