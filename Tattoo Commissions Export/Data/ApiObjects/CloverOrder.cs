﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data
{
    [DataContract]
    public class CloverOrder : CloverObject
    {
        [DataMember(Name = "currency", EmitDefaultValue = false)]
        public string Currency { get; set; }
        [DataMember(Name = "employee", EmitDefaultValue = false)]
        public Reference Employee { get; set; }
        [DataMember(Name = "total", EmitDefaultValue = false)]
        public int? Total { get; set; }
        [DataMember(Name = "state", EmitDefaultValue = false)]
        public string State { get; set; }
    }
}
