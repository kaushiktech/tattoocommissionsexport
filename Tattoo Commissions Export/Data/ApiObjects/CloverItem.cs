﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.ApiObjects
{
    [DataContract]
    public class CloverItem : CloverObject
    {
        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }
        [DataMember(Name = "price", EmitDefaultValue = false)]
        public string Price { get; set; }
        [DataMember(Name = "priceType", EmitDefaultValue = false)]
        public string PriceType { get; set; }
    }        
}
