﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.ApiObjects
{
    [DataContract]
    public class CloverTender : CloverObject
    {
        [DataMember(Name = "label", EmitDefaultValue = false)]
        public String Label { get; set; }
        [DataMember(Name = "opensCashDrawer", EmitDefaultValue = false)]
        public bool OpensCashDrawer { get; set; }
        [DataMember(Name = "enabled", EmitDefaultValue = false)]
        public bool Enabled { get; set; }
        [DataMember(Name = "visible", EmitDefaultValue = false)]
        public bool Visible { get; set; }
    }
}
