﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Tattoo_Commissions_Export.Data
{
    [DataContract]
    public class CloverObject
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public string Id { get; set; }
        [DataMember(Name = "createdTime", EmitDefaultValue = false)]
        public long CreatedTime { get; set; }

    }
    public class CloverWebResponse<T>
    {
        public IEnumerable<T> elements { get; set; }
        public string href { get; set; }
    }
}
