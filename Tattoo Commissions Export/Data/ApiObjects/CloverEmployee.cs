﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data
{
    [DataContract]
    public class CloverEmployee : CloverObject
    {
        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }
        [DataMember(Name = "nickname", EmitDefaultValue = false)]
        public string Nickname { get; set; }
        [DataMember(Name = "pin", EmitDefaultValue = false)]
        public double Pin { get; set; }
        [DataMember(Name = "role", EmitDefaultValue = false)]
        public string Role { get; set; }
        [DataMember(Name = "isOwner", EmitDefaultValue = false)]
        public bool IsOwner { get; set; }
    }
}
