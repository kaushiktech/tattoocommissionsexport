﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Tattoo_Commissions_Export.Data.ApiObjects
{
    [DataContract]
    public class CloverPayment : CloverObject
    {
        [DataMember(Name = "order", EmitDefaultValue = false)]
        public Reference Order { get; set; }
        [DataMember(Name = "tender", EmitDefaultValue = false)]
        public Reference Tender { get; set; }
    }
}
