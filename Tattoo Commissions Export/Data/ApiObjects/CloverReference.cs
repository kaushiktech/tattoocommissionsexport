﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data
{
    [DataContract]
    public class Reference
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public string Id { get; set; }
    }
}
