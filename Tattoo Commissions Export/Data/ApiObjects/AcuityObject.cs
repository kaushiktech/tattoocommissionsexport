﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.ApiObjects
{
    [DataContract]
    public class AcuityObject
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public string Id { get; set; }
        [DataMember(Name = "dateCreated", EmitDefaultValue = false)]
        public DateTime CreatedTime { get; set; }

    }
    public class AcuityWebResponse<T>
    {
        public IEnumerable<T> elements { get; set; }
        public string href { get; set; }
    }
}
