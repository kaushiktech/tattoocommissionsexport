﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Tattoo_Commissions_Export.Data
{
    [XmlRoot("Acuity")]
    public class AcuitySettings
    {
        [XmlArray("Users")]
        [XmlArrayItem("User")]
        public List<User> Users { get; set; }
        public string ApiUrl { get; set; }
        public int Limit { get; set; }
    }
    public class User
    {
        public string Id { get; set; }
        public string ApiKey { get; set; }
    }
}
