﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class Tender
    {
        [Browsable(false)]
        public string Id { get; set; }
        public String Label { get; set; }
        
        public String MerchantId { get; set; }
        [Browsable(false)]
        public bool OpensCashDrawer { get; set; }
        [Browsable(false)]
        public bool Enabled { get; set; }
        [Browsable(false)]
        public bool Visible { get; set; }
        public int PayType { get; set; }
    }
}
