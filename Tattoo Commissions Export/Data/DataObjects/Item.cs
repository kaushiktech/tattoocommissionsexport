﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class Item
    {
        [Browsable(false)]
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        [DisplayName("Price Type")]
        public string PriceType { get; set; }
        public long Category { get; set; }
        
        public string MerchantId { get; set; }
    }
}
