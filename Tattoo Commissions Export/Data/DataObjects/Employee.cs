﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class Employee
    {
        public string Id { get; set; }
        
        public string Name { get; set; }
        
        public string Nickname { get; set; }
        
        public double Pin { get; set; }
        
        public string Role { get; set; }
        
        public bool IsOwner { get; set; }
    }
}
