﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    
    public class Payment :Model<String>
    {
        public string OrderId { get; set; }        
        public string TenderId { get; set; }
        public DateTimeOffset CreatedTime { get; set; }
    }
}
