﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class Appointment
    {
        public string Id { get; set; }
        [Browsable(false)]
        public DateTime CreatedTime { get; set; }
        public DateTime Date { get; set; }
        public decimal Price { get; set; }
       
        public string ArtistName { get; set; }
        
        public string Paid { get; set; }
        
        public string Type { get; set; }
        
        public string Category { get; set; }
    }
}
