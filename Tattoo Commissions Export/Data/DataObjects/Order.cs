﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class Order : Model<string>
    {
        public string Currency { get; set; }
        public string EmployeeId { get; set; }
        public decimal? Total { get; set; }
        public string Title { get; set; }
        public int? OrderStatusId { get; set; }
        public DateTimeOffset CreatedTime { get; set; }
    }
}

