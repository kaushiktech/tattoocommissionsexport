﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class CalculatedData
    {
        public string ArtistName { get; set; }
        [Browsable(false)]
        public string ItemId { get; set; }
        public DateTimeOffset Date { get; set; }
        public decimal Payment { get; set; }
        [Browsable(false)]
        public long CategoryId { get; set; }
        public string Name { get; set; }
        [Browsable(false)]
        public int PaymentTypeId { get; set; }
        [Browsable(false)]
        public string PaymentType
        {
            get
            {
                if (PaymentTypeId == 1)
                    return "Cash";
                else if (PaymentTypeId == 2 || CategoryId == 5)
                    return "Credit";
                else
                    return "Other";
            }
        }
        public bool IsArtistGone { get; set; }
        public string Category
        {
            get
            {
                return Utility.GetCategory(CategoryId);
            }
        }
        public decimal Commission { get; set; }
    }
}
