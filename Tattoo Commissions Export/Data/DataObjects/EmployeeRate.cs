﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class EmployeeRate
    {
        [Browsable(false)]
        public string Id { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Walk-in Rate")]
        public long WalkInRate { get; set; }
        [DisplayName("Personal Client Rate")]
        public long PersonalClientRate { get; set; }
        [DisplayName("Merchandise Rate")]
        public long MerchandiseRate { get; set; }
        [DisplayName("Piercing Rate")]
        public long PiercingRate { get; set; }
        [DisplayName("Acuity Rate")]
        public long AcuityRate { get; set; }
    }
}
