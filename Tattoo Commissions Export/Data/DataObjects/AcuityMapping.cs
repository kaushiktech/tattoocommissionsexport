﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class AcuityMapping
    {
        [Browsable(false)]
        public long Id { get; set; }
        public string MerchantId { get; set; }
        public string AcuityType { get; set; }
    }
}
