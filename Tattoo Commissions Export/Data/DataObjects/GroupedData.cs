﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class GroupedData
    {
        public string Name { get; set; }
        public string ArtistName { get; set; }
        public string Price { get; set; }
        public int CategoryId { get; set; }
        public string Category
        {
            get
            {
                return Utility.GetCategory(CategoryId);
            }
        }
    }
}
