﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tattoo_Commissions_Export.Data.DataObjects
{
    public class LineItem : Model<string>
    {
        public DateTimeOffset CreatedTime { get; set; }
        public string OrderId { get; set; }
        public string ItemId { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public int? UnitQuantity { get; set; }
        public bool? IsRefunded { get; set; }
        public bool? IsRevenue { get; set; }
        public double? QuantitySold { get; set; }
    }
}
