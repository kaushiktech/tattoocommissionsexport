﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Tattoo_Commissions_Export.Data
{   [XmlRoot]
    public class CloverSettings
    {
        public string ApiUrl { get; set; }
        public string FilterApiUrl { get; set; }
        [XmlArray("Merchants")]
        [XmlArrayItem("Merchant")]
        public List<Merchant> MerchantList { get; set; }
        public int Limit { get; set; }
    }
    public class Merchant
    {
        public string Name { get; set; }
        public string ApiSecret { get; set; }
        public string Id { get; set; }
    }
}
