﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tattoo_Commissions_Export.ApiImplementation;
using Tattoo_Commissions_Export.Data;
using Tattoo_Commissions_Export.Data.DataObjects;
using Tattoo_Commissions_Export.DBImplementation;

namespace Tattoo_Commissions_Export
{
    public partial class frmTattoo : Form
    {
        CloverSettings _cloverSettings;
        AcuitySettings _acuitySettings;
        List<EmployeeRate> employeeRates;
        public frmTattoo()
        {
            InitializeComponent();
        }

        private void frmTattoo_Load(object sender, EventArgs e)
        {
            _cloverSettings = Utility.DeserializeXMLFileToObject<CloverSettings>("CloverConfig.xml");
            _acuitySettings = Utility.DeserializeXMLFileToObject<AcuitySettings>("AcuitySchedulingConfig.xml");
            CloverData.MerchantIndex = 0;
            CloverData.Settings = _cloverSettings;
            AcuityData.Settings = _acuitySettings;
        }


        private void btnLoadData_Click(object sender, EventArgs e)
        {
            IList<Tender> tenders = DBFunc.FetchCloverTenders();
            IList<Employee> employees = CloverData.FetchCloverEmployees();
            List<Item> items = DBFunc.FetchCloverInventory();
            employeeRates = DBFunc.FetchEmployeeRates();

            DateTimeOffset stDateOffSet = new DateTimeOffset(
                dtStartTime.Value.Year,
                dtStartTime.Value.Month,
                dtStartTime.Value.Day,
                0,
                0,
                0, new TimeSpan(0, 0, 0));
            DateTimeOffset endDateOffSet = new DateTimeOffset(
                dtEndTime.Value.Year,
                dtEndTime.Value.Month,
                dtEndTime.Value.Day,
                23,
                59,
                59, new TimeSpan(0, 0, 0));
            DateTime stdate = new DateTime(
                 dtStartTime.Value.Year,
                dtStartTime.Value.Month,
                dtStartTime.Value.Day,
                0,
                0,
                0);
            DateTime endDate = new DateTime(
                 dtEndTime.Value.Year,
                dtEndTime.Value.Month,
                dtEndTime.Value.Day,
                23,
                59,
                59);
            IList<Order> orders = CloverData.GetOrders(stDateOffSet, endDateOffSet);
            IList<Payment> payments = CloverData.GetPayments(stDateOffSet, endDateOffSet);
            IList<LineItem> lineItems = CloverData.GetLineItem(stDateOffSet, endDateOffSet);

            IList<Appointment> appList = AcuityData.GetAppointments(stdate, endDate);
            DataTable calcTable = Utility.ToDataTable(GenerateAggregatedData(orders, lineItems, employees, items, appList, employeeRates, payments, tenders));
            string artistName = string.Empty;
            decimal sum = 0;
            bool lastRow = false;
            for (int i = 0; i <= calcTable.Rows.Count; i++)
            {
                if ((i != 0 && i < calcTable.Rows.Count && artistName != calcTable.Rows[i]["ArtistName"].ToString()) || i == calcTable.Rows.Count)
                {
                    lastRow = i == calcTable.Rows.Count;
                    DataRow newRow = calcTable.NewRow();
                    newRow["Category"] = "Total for " + artistName;
                    newRow["Commission"] = sum;
                    calcTable.Rows.InsertAt(newRow, i);
                    if (!lastRow)
                        i++;
                    else
                        break;
                    sum = 0;
                }
                artistName = calcTable.Rows[i]["ArtistName"].ToString();
                sum += Convert.ToDecimal(calcTable.Rows[i]["Commission"]);
            }
            dgView.DataSource = calcTable;
            if (calcTable.Rows.Count > 0)
            {
                dgView.Show();
                btnExport.Enabled = true;
                btnExportDetail.Enabled = true;
            }
            else
            {
                btnExport.Enabled = false;
                btnExportDetail.Enabled = true;
            }
            dgView.AllowUserToAddRows = false;
            dgView.ReadOnly = true;
            dgView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgView.Columns["CategoryId"].Visible = false;
            dgView.Columns["PaymentTypeId"].Visible = false;
            dgView.Columns["IsArtistGone"].Visible = false;
            dgView.Columns["ItemId"].Visible = false;
            dgView.Columns["Commission"].DefaultCellStyle.Format = "$ 0.00";
            dgView.Columns["Payment"].DefaultCellStyle.Format = "$ 0\\";
            dgView.Columns["Date"].DefaultCellStyle.Format = "dd/MMM/yyyy";
        }
        private List<CalculatedData> GenerateAggregatedData(IList<Order> orders, IList<LineItem> lineItems, IList<Employee> employees, List<Item> items, IList<Appointment> appList, List<EmployeeRate> employeeRates, IList<Payment> payments, IList<Tender> tenders)
        {
            List<CalculatedData> calcData = new List<CalculatedData>();
            if (orders.Count > 0 && lineItems.Count > 0)


                calcData = (from or in orders
                            join line in lineItems on or.Id equals line.OrderId
                            join emp in employees on or.EmployeeId equals emp.Id
                            join pay in payments on or.Id equals pay.OrderId
                            join tnd in tenders on pay.TenderId equals tnd.Id
                            select new CalculatedData
                            {
                                Date = line.CreatedTime,
                                ArtistName = emp.Name.Split(' ')[0].ToLower(),
                                Payment = line.Price ?? 0,
                                ItemId = line.ItemId,
                                PaymentTypeId = tnd.PayType,
                                IsArtistGone = false
                            }).ToList();

            foreach (CalculatedData data in calcData)
            {
                if (items.Any(item => item.Id == data.ItemId))
                {
                    data.CategoryId = items.First(item => item.Id == data.ItemId).Category;
                    data.Name = items.First(item => item.Id == data.ItemId).Name;
                }
                else
                {
                    data.CategoryId = 0;
                    data.Name = "Custom";
                }
            }
            if (appList.Count > 0)
            {
                var acuityData = (from acu in appList
                                  where !String.IsNullOrEmpty(acu.ArtistName)
                                  select new CalculatedData
                                  {
                                      ArtistName = String.IsNullOrEmpty(acu.ArtistName) ? "Deleted Calendar" : acu.ArtistName.ToLower(),
                                      CategoryId = 5,
                                      Date = acu.Date,
                                      Name = acu.Type,
                                      Payment = acu.Price,
                                      IsArtistGone = String.IsNullOrEmpty(acu.ArtistName) ? true : false
                                  }).ToList();
                calcData.AddRange(acuityData);
                RunCalculations(calcData);
                calcData = calcData.OrderBy(o => o.ArtistName).ThenBy(t => t.Date).ToList();
            }
            return calcData;
        }
        private void RunCalculations(List<CalculatedData> calcData)
        {
            decimal defaultPerc = 50;
            foreach (CalculatedData data in calcData)
            {
                decimal x = 0, y = 0, z = 0;
                switch (data.CategoryId)
                {
                    case 1:
                        if (data.PaymentTypeId == 1)
                            x = 0.9m;
                        else
                            x = 1;
                        if (data.Payment > 75)
                            y = 10;
                        else
                            y = 0;
                        decimal perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().PersonalClientRate;
                        if (perc > 0)
                            data.Commission = x * (data.Payment - y) * (perc / (decimal)100);
                        break;
                    case 2:
                        if (data.PaymentTypeId == 1)
                            x = 0.9m;
                        else
                            x = 1;
                        y = 5;
                        perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().PiercingRate;
                        if (perc > 0)
                            data.Commission = ((x * data.Payment) - y) * (perc / (decimal)100);
                        break;
                    case 3:
                        if (data.PaymentTypeId == 1)
                            x = 0.9m;
                        else
                            x = 1;
                        perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().MerchandiseRate;
                        if (perc > 0)
                            data.Commission = (x * data.Payment) * (perc / (decimal)100);
                        break;
                    case 5:
                        x = 5;
                        perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().AcuityRate;
                        if (perc > 0)
                            data.Commission = (data.Payment - x) * (perc / (decimal)100);
                        break;
                    case 6:
                        if (data.PaymentTypeId == 0)
                            x = 0.9m;
                        else
                            x = 1;
                        if (data.Payment > 75)
                            y = 10;
                        else
                            y = 0;
                        perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().WalkInRate;
                        if (perc > 0)
                            data.Commission = x * (data.Payment - y) * (perc / (decimal)100);
                        break;
                    default:
                        data.Commission = 0;
                        break;

                }
            }
        }
        private void btnEditInventory_Click(object sender, EventArgs e)
        {
            frmEditConfiguration frmEditConfiguration = new frmEditConfiguration();
            frmEditConfiguration.Mode = "Inventory";
            frmEditConfiguration.MerchantId = _cloverSettings.MerchantList[0].Id;
            frmEditConfiguration.Show();

        }

        private void btnEmployeeRates_Click(object sender, EventArgs e)
        {
            frmEditConfiguration frmEditConfiguration = new frmEditConfiguration();
            frmEditConfiguration.Mode = "EmployeeRates";
            frmEditConfiguration.MerchantId = _cloverSettings.MerchantList[0].Id;
            frmEditConfiguration.Show();
        }

        private void btnEditTenders_Click(object sender, EventArgs e)
        {
            frmEditConfiguration frmEditConfiguration = new frmEditConfiguration();
            frmEditConfiguration.Mode = "Tender";
            frmEditConfiguration.MerchantId = _cloverSettings.MerchantList[0].Id;
            frmEditConfiguration.Show();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in dgView.Columns)
            {
                dt.Columns.Add(col.Name);
            }

            foreach (DataGridViewRow row in dgView.Rows)
            {
                DataRow dRow = dt.NewRow();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRow[cell.ColumnIndex] = cell.Value;
                }
                dt.Rows.Add(dRow);
            }
            int count = dt.Rows.Count;
            for (int i = 0; i < count; i++)
            {
                if (dt.Rows[i]["ArtistName"].ToString() != "")
                {
                    dt.Rows.Remove(dt.Rows[i]);
                    count--;
                    i--;
                }
            }

            dt.Columns.Remove("IsArtistGone");
            dt.Columns.Remove("PaymentTypeId");
            dt.Columns.Remove("CategoryId");
            dt.Columns.Remove("ItemId");
            dt.Columns.Remove("Date");
            dt.Columns.Remove("Payment");
            dt.Columns.Remove("Name");
            dt.Columns.Remove("ArtistName");
            dt.Columns.Remove("PaymentType");
            dt = ConvertDataTable(dt);
            dt.Columns["Commission"].Convert(val => string.IsNullOrEmpty(val.ToString()) ? "" : Convert.ToDecimal(val).ToString("$ 0.00"));
            dt.Columns["Category"].ColumnName = "Totals";
            try
            {
                Utility.Export2CSV(dt, AppDomain.CurrentDomain.BaseDirectory + "export.csv");
                Process.Start(AppDomain.CurrentDomain.BaseDirectory + "export.csv");
            }
            catch (Exception exc)
            {
                MessageBox.Show("export.csv might be open. Please close all instances of this file before proceeding.");
            }
        }
        private DataTable ConvertDataTable(DataTable data)
        {
            DataTable dtClone = data.Clone(); //just copy structure, no data
            for (int i = 0; i < dtClone.Columns.Count; i++)
            {
                if (dtClone.Columns[i].DataType != typeof(string))
                    dtClone.Columns[i].DataType = typeof(string);
            }

            foreach (DataRow dr in data.Rows)
            {
                dtClone.ImportRow(dr);
            }
            return dtClone;
        }

        private void btnExportDetail_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in dgView.Columns)
            {
                dt.Columns.Add(col.Name);
            }

            foreach (DataGridViewRow row in dgView.Rows)
            {
                DataRow dRow = dt.NewRow();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRow[cell.ColumnIndex] = cell.Value;
                }
                dt.Rows.Add(dRow);
            }
            dt.Columns.Remove("IsArtistGone");
            dt.Columns.Remove("PaymentTypeId");
            dt.Columns.Remove("CategoryId");
            dt.Columns.Remove("ItemId");
            dt = ConvertDataTable(dt);
            dt.Columns["Date"].Convert(val => string.IsNullOrEmpty(val.ToString()) ? "" : DateTimeOffset.Parse(val.ToString()).ToString("dd/MMM/yyyy"));
            dt.Columns["Payment"].Convert(val => string.IsNullOrEmpty(val.ToString()) ? "" : Convert.ToDecimal(val).ToString("C"));
            dt.Columns["Commission"].Convert(val => string.IsNullOrEmpty(val.ToString()) ? "" : Convert.ToDecimal(val).ToString("C"));
            try
            {
                Utility.Export2CSV(dt, AppDomain.CurrentDomain.BaseDirectory + "exportDetails.csv");
                Process.Start(AppDomain.CurrentDomain.BaseDirectory + "exportDetails.csv");
            }
            catch (Exception exc)
            {
                MessageBox.Show("exportDetails.csv might be open. Please close all instances of this file before proceeding.");
            }
        }
    }
}