﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tattoo_Commissions_Export.ApiImplementation;
using Tattoo_Commissions_Export.Data;
using Tattoo_Commissions_Export.Data.DataObjects;
using Tattoo_Commissions_Export.DBImplementation;

namespace Tattoo_Commissions_Export
{
    public partial class TattooFormV2 : Form
    {
        CloverSettings _cloverSettings;
        AcuitySettings _acuitySettings;
        List<EmployeeRate> employeeRates;
        public TattooFormV2()
        {
            InitializeComponent();
        }

        private void TattooFormV2_Load(object sender, EventArgs e)
        {
            _cloverSettings = Utility.DeserializeXMLFileToObject<CloverSettings>("CloverConfig.xml");
            _acuitySettings = Utility.DeserializeXMLFileToObject<AcuitySettings>("AcuitySchedulingConfig.xml");
            this.Height = Screen.PrimaryScreen.Bounds.Height;
            CloverData.Settings = _cloverSettings;
            AcuityData.Settings = _acuitySettings;
            foreach (Merchant merchant in _cloverSettings.MerchantList)
            {
                storeTabControl.TabPages.Add(merchant.Name);
                storeTabControl.SelectedIndexChanged += new EventHandler(LoadControls);
            }

            LoadControls();
        }
        private void LoadControls(Object sender, EventArgs e)
        {
            LoadControls();
        }
        private void Validate()
        {
            int count = 0;
            foreach (TabPage tabpage in storeTabControl.TabPages)
            {
                if (tabpage.Controls.Find("dgView", true).Any())
                {
                    if (((DataGridView)tabpage.Controls.Find("dgView", true).First()).RowCount > 0)
                    {
                        count++;
                    }
                }
            }
            if (count > 0)
            {
                btnGlobalExport.Enabled = true;
                btnGlobalExportDetails.Enabled = true;
            }
        }
        private void LoadControls()
        {

            TabPage currentTab = storeTabControl.SelectedTab;

            Label lblDateFrom = new Label();
            lblDateFrom.Text = "Start Date";
            lblDateFrom.Location = new Point(10, 36);
            lblDateFrom.AutoSize = true;

            DateTimePicker dtFrom = new DateTimePicker();
            dtFrom.Location = new Point(80, 31);
            dtFrom.Name = "dtFrom";
            dtFrom.Width = 150;
            dtFrom.Format = DateTimePickerFormat.Short;

            Label lblDateTo = new Label();
            lblDateTo.Text = "End Date";
            lblDateTo.AutoSize = true;
            lblDateTo.Location = new Point(250, 36);

            DateTimePicker dtTo = new DateTimePicker();
            dtTo.Location = new Point(330, 31);
            dtTo.Name = "dtTo";
            dtTo.Width = 150;
            dtTo.Format = DateTimePickerFormat.Short;

            Button btnEditInv = new Button();
            btnEditInv.Name = "btnEditInv";
            btnEditInv.Size = new Size(145, 35);
            btnEditInv.Text = "Edit Inventory";
            btnEditInv.Location = new Point(10, 71);
            btnEditInv.Click += btnEditInventory_Click;

            Button btnEditTend = new Button();
            btnEditTend.Name = "btnEditTend";
            btnEditTend.Size = new Size(145, 35);
            btnEditTend.Text = "Edit Tenders";
            btnEditTend.Location = new Point(160, 71);
            btnEditTend.Click += btnEditTenders_Click;

            //Button btnEditRates = new Button();
            //btnEditRates.Name = "btnEditRates";
            //btnEditRates.Location = new Point(315, 71);
            //btnEditRates.Size = new Size(145, 35);
            //btnEditRates.Text = "Edit Employee Rates";
            //btnEditRates.Click += btnEmployeeRates_Click;

            Button btnSearch = new Button();
            btnSearch.Name = "btnSearch";
            btnSearch.Location = new Point(200, 121);
            btnSearch.Size = new Size(100, 35);
            btnSearch.Text = "Search";
            btnSearch.Click += btnLoadData_Click;

            Button btnExport = new Button();
            btnExport.Name = "btnExport";
            btnExport.Location = new Point(400, 121);
            btnExport.Size = new Size(100, 35);
            btnExport.Text = "Export";
            btnExport.Click += btnExport_Click;
            btnExport.Enabled = false;

            Button btnExportDetails = new Button();
            btnExportDetails.Name = "btnExportDetails";
            btnExportDetails.Location = new Point(500, 121);
            btnExportDetails.Size = new Size(100, 35);
            btnExportDetails.Text = "Export Details";
            btnExportDetails.Click += btnExportDetail_Click;
            btnExportDetails.Enabled = false;

            DataGridView dgView = new DataGridView();

            dgView.Anchor = ((AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top )
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgView.Location = new Point(12, 165);
            dgView.Name = "dgView";
            dgView.RowTemplate.Height = 28;
            dgView.Size = new Size(620, 250);
            dgView.AutoSize = false;
            dgView.Margin = new Padding(0, 0, 0, 30);
            dgView.TabIndex = 5;



            currentTab.Controls.Add(lblDateFrom);
            currentTab.Controls.Add(dtFrom);
            currentTab.Controls.Add(lblDateTo);
            currentTab.Controls.Add(dtTo);
            currentTab.Controls.Add(btnEditInv);
            
            currentTab.Controls.Add(btnEditTend);
            currentTab.Controls.Add(btnSearch);
            currentTab.Controls.Add(btnExport);
            currentTab.Controls.Add(btnExportDetails);
            currentTab.Controls.Add(dgView);
        }
        private void RunCalculations(List<CalculatedData> calcData)
        {
            decimal defaultPerc = 50;
            foreach (CalculatedData data in calcData)
            {
                decimal x = 0, y = 0, z = 0;
                switch (data.CategoryId)                {
                    
                    case 1:
                        if (data.PaymentTypeId == 1)
                            x = 0.9m;
                        else
                            x = 1;
                        if (data.Payment > 75)
                            y = 10;
                        else
                            y = 0;
                        decimal perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().PersonalClientRate;
                        if (perc > 0)
                            data.Commission = x * (data.Payment - y) * (perc / (decimal)100);
                        break;
                    case 2:
                        if (data.PaymentTypeId == 1)
                            x = 0.9m;
                        else
                            x = 1;
                        y = 5;
                        perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().PiercingRate;
                        if (perc > 0)
                            data.Commission = ((x * data.Payment) - y) * (perc / (decimal)100);
                        break;
                    case 3:
                        if (data.PaymentTypeId == 1)
                            x = 0.9m;
                        else
                            x = 1;
                        perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().MerchandiseRate;
                        if (perc > 0)
                            data.Commission = (x * data.Payment) * (perc / (decimal)100);
                        break;
                    case 4:
                        x = 5;
                        perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().AcuityRate;
                        if (perc > 0)
                            data.Commission = (data.Payment - x) * (perc / (decimal)100);
                        break;
                    case 5:
                        if (data.PaymentTypeId == 0)
                            x = 0.9m;
                        else
                            x = 1;
                        if (data.Payment > 75)
                            y = 10;
                        else
                            y = 0;
                        perc = defaultPerc;
                        if (employeeRates.Any(emp => emp.Name.Contains(data.ArtistName)))
                            perc = employeeRates.Where(emp => emp.Name.Contains(data.ArtistName)).First().WalkInRate;
                        if (perc > 0)
                            data.Commission = x * (data.Payment - y) * (perc / (decimal)100);
                        break;
                    default:
                        data.Commission = 0;
                        break;

                }
            }
        }
        private void btnEditInventory_Click(object sender, EventArgs e)
        {
            frmEditConfiguration frmEditConfiguration = new frmEditConfiguration();
            frmEditConfiguration.Mode = "Inventory";
            frmEditConfiguration.Merchant = _cloverSettings.MerchantList[storeTabControl.SelectedIndex];
            frmEditConfiguration.Show();
        }

        private void btnEmployeeRates_Click(object sender, EventArgs e)
        {
            frmEditConfiguration frmEditConfiguration = new frmEditConfiguration();
            frmEditConfiguration.Mode = "EmployeeRates";
            frmEditConfiguration.Merchant = _cloverSettings.MerchantList[storeTabControl.SelectedIndex];
            frmEditConfiguration.Show();
        }

        private void btnEditTenders_Click(object sender, EventArgs e)
        {
            frmEditConfiguration frmEditConfiguration = new frmEditConfiguration();
            frmEditConfiguration.Mode = "Tender";
            frmEditConfiguration.Merchant = _cloverSettings.MerchantList[storeTabControl.SelectedIndex];
            frmEditConfiguration.Show();
        }
        private DataTable GetDataTableFromDataGrid(DataGridView dgView)
        {
            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in dgView.Columns)
            {
                dt.Columns.Add(col.Name);
            }

            foreach (DataGridViewRow row in dgView.Rows)
            {
                DataRow dRow = dt.NewRow();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRow[cell.ColumnIndex] = cell.Value;
                }
                dt.Rows.Add(dRow);
            }
            int count = dt.Rows.Count;
            for (int i = 0; i < count; i++)
            {
                if (dt.Rows[i]["ArtistName"].ToString() != "")
                {
                    dt.Rows.Remove(dt.Rows[i]);
                    count--;
                    i--;
                }
            }

            dt.Columns.Remove("IsArtistGone");
            dt.Columns.Remove("PaymentTypeId");
            dt.Columns.Remove("CategoryId");
            dt.Columns.Remove("ItemId");
            dt.Columns.Remove("Date");
            dt.Columns.Remove("Payment");
            dt.Columns.Remove("Name");
            dt.Columns.Remove("ArtistName");
            dt.Columns.Remove("PaymentType");
            dt = ConvertDataTable(dt);
            dt.Columns["Commission"].Convert(val => string.IsNullOrEmpty(val.ToString()) ? "" : Convert.ToDecimal(val).ToString("$ 0.00"));
            dt.Columns["Category"].ColumnName = "Totals";
            return dt;
        }
        private void btnExport_Click(object sender, EventArgs e)
        {
            DataGridView dgView = (DataGridView)storeTabControl.TabPages[storeTabControl.SelectedIndex].Controls.Find("dgView", true).First();
            DataTable dt = GetDataTableFromDataGrid(dgView);
            try
            {
                Utility.Export2CSV(dt, AppDomain.CurrentDomain.BaseDirectory + "export.csv");
                Process.Start(AppDomain.CurrentDomain.BaseDirectory + "export.csv");
            }
            catch (Exception exc)
            {
                MessageBox.Show("export.csv might be open. Please close all instances of this file before proceeding.");
            }
        }
        private void btnLoadData_Click(object sender, EventArgs e)
        {

            string merchantId = _cloverSettings.MerchantList[storeTabControl.SelectedIndex].Id;
            CloverData.MerchantId = merchantId;
            IList<Tender> tenders = DBFunc.FetchCloverTenders(merchantId);
            IList<Employee> employees = CloverData.FetchCloverEmployees();
            List<Item> items = DBFunc.FetchCloverInventory(merchantId);
            employeeRates = DBFunc.FetchEmployeeRates();
            foreach(EmployeeRate rate in employeeRates)
            {
                rate.Name = rate.Name.ToLower();
            }
            DateTimePicker dtFrom = (DateTimePicker)storeTabControl.TabPages[storeTabControl.SelectedIndex].Controls.Find("dtFrom", true).First();
            DateTimePicker dtTo = (DateTimePicker)storeTabControl.TabPages[storeTabControl.SelectedIndex].Controls.Find("dtTo", true).First();

            

            DateTimeOffset stDateOffSet = new DateTimeOffset(
                dtFrom.Value.Year,
                dtFrom.Value.Month,
                dtFrom.Value.Day,
                0,
                0,
                0, new TimeSpan(0, 0, 0));
            DateTimeOffset endDateOffSet = new DateTimeOffset(
                dtTo.Value.Year,
                dtTo.Value.Month,
                dtTo.Value.Day,
                23,
                59,
                59, new TimeSpan(0, 0, 0));
            DateTime stdate = new DateTime(
                 dtFrom.Value.Year,
                dtFrom.Value.Month,
                dtFrom.Value.Day,
                0,
                0,
                0);
            DateTime endDate = new DateTime(
                 dtTo.Value.Year,
                dtTo.Value.Month,
                dtTo.Value.Day,
                23,
                59,
                59);
            IList<Order> orders = CloverData.GetOrders(stDateOffSet, endDateOffSet);
            IList<Payment> payments = CloverData.GetPayments(stDateOffSet, endDateOffSet);
            IList<LineItem> lineItems = CloverData.GetLineItem(stDateOffSet, endDateOffSet);

            IList<Appointment> appListTemp = AcuityData.GetAppointments(stdate, endDate);
            IList<AcuityMapping> mapList = DBFunc.FetchAcuityMapping();
            foreach (String mapping in appListTemp.Select(t=>t.Type).Distinct())
            {
                if (mapList.Count == 0 || mapList.Count(i => i.AcuityType == mapping) == 0)
                    DBFunc.InsertAcuityMapping(new AcuityMapping { AcuityType = mapping, MerchantId = "" });
            }
            mapList = DBFunc.FetchAcuityMapping(merchantId);
            List<Appointment> appList= new List<Appointment>();
            foreach (AcuityMapping mapping in mapList.Where(m=>m.MerchantId==merchantId))
            {
                if(appListTemp.Any(app=>app.Type==mapping.AcuityType))
                {
                    appList.AddRange(appListTemp.Where(app => app.Type == mapping.AcuityType));
                }
            }
            DataTable calcTable = Utility.ToDataTable(GenerateAggregatedData(orders, lineItems, employees, items, appList, employeeRates, payments, tenders));
            string artistName = string.Empty;
            decimal sum = 0;
            bool lastRow = false;
            for (int i = 0; i <= calcTable.Rows.Count; i++)
            {
                if ((i != 0 && i < calcTable.Rows.Count && artistName != calcTable.Rows[i]["ArtistName"].ToString()) || i == calcTable.Rows.Count)
                {
                    lastRow = i == calcTable.Rows.Count;
                    DataRow newRow = calcTable.NewRow();
                    newRow["Category"] = "Total for " + artistName;
                    newRow["Commission"] = sum;
                    calcTable.Rows.InsertAt(newRow, i);
                    if (!lastRow)
                        i++;
                    else
                        break;
                    sum = 0;
                }
                artistName = calcTable.Rows[i]["ArtistName"].ToString();
                sum += Convert.ToDecimal(calcTable.Rows[i]["Commission"]);
            }
            DataGridView dgView = (DataGridView)storeTabControl.TabPages[storeTabControl.SelectedIndex].Controls.Find("dgView", true).First();
            dgView.DataSource = calcTable;
            Button btnExport = (Button)storeTabControl.TabPages[storeTabControl.SelectedIndex].Controls.Find("btnExport", true).First();
            Button btnExportDetail = (Button)storeTabControl.TabPages[storeTabControl.SelectedIndex].Controls.Find("btnExportDetails", true).First();
            if (calcTable.Rows.Count > 0)
            {
                dgView.Show();
                btnExport.Enabled = true;
                btnExportDetail.Enabled = true;
            }
            else
            {
                btnExport.Enabled = false;
                btnExportDetail.Enabled = true;
            }
            dgView.AllowUserToAddRows = false;
            dgView.ReadOnly = true;
            dgView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgView.Columns["CategoryId"].Visible = false;
            dgView.Columns["PaymentTypeId"].Visible = false;
            dgView.Columns["IsArtistGone"].Visible = false;
            dgView.Columns["ItemId"].Visible = false;
            dgView.Columns["Commission"].DefaultCellStyle.Format = "$ 0.00";
            dgView.Columns["Payment"].DefaultCellStyle.Format = "$ 0\\";
            dgView.Columns["Date"].DefaultCellStyle.Format = "dd/MMM/yyyy";
            Validate();
        }
        private DataTable ConvertDataTable(DataTable data)
        {
            DataTable dtClone = data.Clone(); //just copy structure, no data
            for (int i = 0; i < dtClone.Columns.Count; i++)
            {
                if (dtClone.Columns[i].DataType != typeof(string))
                    dtClone.Columns[i].DataType = typeof(string);
            }

            foreach (DataRow dr in data.Rows)
            {
                dtClone.ImportRow(dr);
            }
            return dtClone;
        }
        private List<CalculatedData> GenerateAggregatedData(IList<Order> orders, IList<LineItem> lineItems, IList<Employee> employees, List<Item> items, IList<Appointment> appList, List<EmployeeRate> employeeRates, IList<Payment> payments, IList<Tender> tenders)
        {
            List<CalculatedData> calcData = new List<CalculatedData>();
            if (orders.Count > 0 && lineItems.Count > 0)


                calcData = (from or in orders
                            join line in lineItems on or.Id equals line.OrderId
                            join emp in employees on or.EmployeeId equals emp.Id
                            join pay in payments on or.Id equals pay.OrderId
                            join tnd in tenders on pay.TenderId equals tnd.Id
                            select new CalculatedData
                            {
                                Date = line.CreatedTime,
                                ArtistName = emp.Name.Split(' ')[0].ToLower(),
                                Payment = line.Price ?? 0,
                                ItemId = line.ItemId,
                                PaymentTypeId = tnd.PayType,
                                IsArtistGone = false
                            }).ToList();

            foreach (CalculatedData data in calcData)
            {
                if (items.Any(item => item.Id == data.ItemId))
                {
                    data.CategoryId = items.First(item => item.Id == data.ItemId).Category;
                    data.Name = items.First(item => item.Id == data.ItemId).Name;
                }
                else
                {
                    data.CategoryId = 0;
                    data.Name = "Other";
                }
            }
            if (appList.Count > 0)
            {
                var acuityData = (from acu in appList
                                  where !String.IsNullOrEmpty(acu.ArtistName)
                                  select new CalculatedData
                                  {
                                      ArtistName = String.IsNullOrEmpty(acu.ArtistName) ? "Deleted Calendar" : acu.ArtistName.ToLower(),
                                      CategoryId = 4,
                                      Date = acu.Date,
                                      Name = acu.Type,
                                      Payment = acu.Price,
                                      IsArtistGone = String.IsNullOrEmpty(acu.ArtistName) ? true : false
                                  }).ToList();
                calcData.AddRange(acuityData);
            }
            RunCalculations(calcData);
            calcData = calcData.OrderBy(o => o.ArtistName).ThenBy(t => t.Date).ToList();
            return calcData;
        }
        private void btnExportDetail_Click(object sender, EventArgs e)
        {
            DataGridView dgView = (DataGridView)storeTabControl.TabPages[storeTabControl.SelectedIndex].Controls.Find("dgView", true).First();
            DataTable dt = GetDataTableFromDataGridDetail(dgView);
            try
            {
                Utility.Export2CSV(dt, AppDomain.CurrentDomain.BaseDirectory + "exportDetails.csv");
                Process.Start(AppDomain.CurrentDomain.BaseDirectory + "exportDetails.csv");
            }
            catch (Exception exc)
            {
                MessageBox.Show("exportDetails.csv might be open. Please close all instances of this file before proceeding.");
            }
        }
        private DataTable GetDataTableFromDataGridDetail(DataGridView dgView)
        {
            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in dgView.Columns)
            {
                dt.Columns.Add(col.Name);
            }

            foreach (DataGridViewRow row in dgView.Rows)
            {
                DataRow dRow = dt.NewRow();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRow[cell.ColumnIndex] = cell.Value;
                }
                dt.Rows.Add(dRow);
            }
            dt.Columns.Remove("IsArtistGone");
            dt.Columns.Remove("PaymentTypeId");
            dt.Columns.Remove("CategoryId");
            dt.Columns.Remove("ItemId");
            dt = ConvertDataTable(dt);
            dt.Columns["Date"].Convert(val => string.IsNullOrEmpty(val.ToString()) ? "" : DateTimeOffset.Parse(val.ToString()).ToString("dd/MMM/yyyy"));
            dt.Columns["Payment"].Convert(val => string.IsNullOrEmpty(val.ToString()) ? "" : Convert.ToDecimal(val).ToString("C"));
            dt.Columns["Commission"].Convert(val => string.IsNullOrEmpty(val.ToString()) ? "" : Convert.ToDecimal(val).ToString("C"));
            return dt;
        }

        private void btnGlobalExport_Click(object sender, EventArgs e)
        {
            int count = 0;
            DataTable dtGlobal = new DataTable();
            foreach (TabPage tabpage in storeTabControl.TabPages)
            {
                if (tabpage.Controls.Find("dgView", true).Any())
                {
                    if (((DataGridView)tabpage.Controls.Find("dgView", true).First()).RowCount > 0)
                    {
                        count++;
                        DataGridView dgView = (DataGridView)tabpage.Controls.Find("dgView", true).First();
                        DataTable dt = GetDataTableFromDataGrid(dgView);
                        DataRow dr = dt.NewRow();
                        dr[0] = "STORE : " + tabpage.Text;
                        dt.Rows.InsertAt(dr, 0);
                        if (count == 1)
                            dtGlobal = dt;
                        else
                        {
                            dtGlobal.Merge(dt);
                        }
                    }
                }
            }
            if (dtGlobal.Rows.Count > 0)
            {
                try
                {
                    Utility.Export2CSV(dtGlobal, AppDomain.CurrentDomain.BaseDirectory + "export.csv");
                    Process.Start(AppDomain.CurrentDomain.BaseDirectory + "export.csv");
                }
                catch (Exception exc)
                {
                    MessageBox.Show("export.csv might be open. Please close all instances of this file before proceeding.");
                }
            }
        }

        private void btnGlobalExportDetails_Click(object sender, EventArgs e)
        {
            int count = 0;
            DataTable dtGlobal = new DataTable();
            foreach (TabPage tabpage in storeTabControl.TabPages)
            {
                if (tabpage.Controls.Find("dgView", true).Any())
                {
                    if (((DataGridView)tabpage.Controls.Find("dgView", true).First()).RowCount > 0)
                    {
                        count++;
                        DataGridView dgView = (DataGridView)tabpage.Controls.Find("dgView", true).First();
                        DataTable dt = GetDataTableFromDataGridDetail(dgView);
                        DataRow dr = dt.NewRow();
                        dr[0] = "STORE : " + tabpage.Text;
                        dt.Rows.InsertAt(dr, 0);
                        if (count == 1)
                            dtGlobal = dt;
                        else
                        {
                            dtGlobal.Merge(dt);
                        }
                    }
                }
            }
            if (dtGlobal.Rows.Count > 0)
            {
                try
                {
                    Utility.Export2CSV(dtGlobal, AppDomain.CurrentDomain.BaseDirectory + "exportDetails.csv");
                    Process.Start(AppDomain.CurrentDomain.BaseDirectory + "exportDetails.csv");
                }
                catch (Exception exc)
                {
                    MessageBox.Show("exportDetails.csv might be open. Please close all instances of this file before proceeding.");
                }
            }
        }

        private void btnAcuityMapping_Click(object sender, EventArgs e)
        {
            frmEditConfiguration frmEditConfiguration = new frmEditConfiguration();
            frmEditConfiguration.Mode = "Acuity";
            //frmEditConfiguration.Merchant = _cloverSettings.MerchantList[storeTabControl.SelectedIndex];
            frmEditConfiguration.Show();
        }

        private void btnEditRates_Click(object sender, EventArgs e)
        {
            btnEmployeeRates_Click(sender, e);
        }
    }
}

